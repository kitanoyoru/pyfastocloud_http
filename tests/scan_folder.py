import unittest

from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class ScanFolder(unittest.TestCase):
    def test_lifetime(self):
        directory = "/home/fastocloud/streamer"
        extensions = ["mp4"]

        resp = client.scan_folder(directory, extensions)

        self.assertNotEqual(resp, None)


if __name__ == "__main__":
    unittest.main()
