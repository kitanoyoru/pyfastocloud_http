import unittest

from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class GetStreamStats(unittest.TestCase):
    def test_lifetime(self):
        id = "id"
        resp = client.get_stream_stats(id=id)
        self.assertNotEqual(resp, None)


if __name__ == "__main__":
    unittest.main()
