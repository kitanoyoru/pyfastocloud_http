import unittest

from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth

endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class RemoveStreamFromDB(unittest.TestCase):
    def test_lifetime(self):
        id = "id"
        resp = client.remove_stream_from_db(id)

        self.assertEqual(resp, None)
