import unittest

from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class GetVersion(unittest.TestCase):
    def test_lifetime(self):
        resp = client.get_version()
        self.assertNotEqual(resp, None)


if __name__ == "__main__":
    unittest.main()
