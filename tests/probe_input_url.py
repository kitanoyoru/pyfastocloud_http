import unittest

from pyfastocloud_base.constants import InputUri

from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth

endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class ProbeInput(unittest.TestCase):
    def test_lifetime(self):
        url = InputUri(id=0, uri="https://www.twitch.tv/some")

        resp = client.probe_in_stream(url)

        self.assertNotEqual(resp, None)


if __name__ == "__main__":
    unittest.main()
