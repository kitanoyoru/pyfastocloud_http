import unittest

from pyfastocloud_base.streams.config import ProxyConfig, ProxyVodConfig
from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)

# TODO: Which type of config should I use


class StartStream(unittest.TestCase):
    def test_proxy_stream(self):
        sid = "234"
        out = {"id": 0, "uri": "https://localhost:1234/master.m3u8"}
        config = ProxyConfig(sid, [out])

        resp = client.clean_stream(config)

        self.assertEqual(resp, None)

    def test_proxy_vod(self):
        sid = "1234"
        out = {"id": 12, "uri": "https://some.tv/live/secure/chunkli1st.m3u8"}
        config = ProxyVodConfig(sid=sid, output=[out])

        resp = client.clean_stream(config)

        self.assertEqual(resp, None)


if __name__ == "__main__":
    unittest.main()
