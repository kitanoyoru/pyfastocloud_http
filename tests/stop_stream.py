import unittest

from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class StopStream(unittest.TestCase):
    def test_lifetime(self):
        id = "6036824ced84ffd2c5e50db0"
        force = False

        resp = client.stop_stream(id, force)

        self.assertEqual(resp, None)


if __name__ == "__main__":
    unittest.main()
