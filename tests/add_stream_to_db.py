import unittest

from pyfastocloud_base.constants import OutputUri, StreamType

from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth


endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class AddStreamToDB(unittest.TestCase):
    def test_lifetime(self):
        id = "id"
        type = StreamType.VOD_PROXY
        out = OutputUri(0, "https://localhost:1234/master.m3u8")

        resp = client.add_stream_to_db(id=id, type=type, output=[out])
        self.assertEqual(resp, None)


if __name__ == "__main__":
    unittest.main()
