import unittest

from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth

endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class ChangeInputSourceStream(unittest.TestCase):
    def test_lifetime(self):
        stream_id = "12355"
        channel_id = 1

        resp = client.change_input_source_stream(stream_id, channel_id)

        self.assertEqual(resp, None)


if __name__ == "__main__":
    unittest.main()
