import unittest

from pyfastocloud_base.constants import InputUri

from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth

endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class ChangeInputSourceStream(unittest.TestCase):
    def test_lifetime(self):
        stream_id = "618f8abc0d6058eb9240e240"
        url = InputUri(id=0, uri="https://www.twitch.tv/some")

        resp = client.inject_master_input_url(stream_id, url)

        self.assertEqual(resp, None)


if __name__ == "__main__":
    unittest.main()
