import unittest

from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth

endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class MountS3Bucket(unittest.TestCase):
    def test_lifetime(self):
        name = "some"
        path = "/home/sasha/folder"
        key = "key"
        secret = "secret"

        resp = client.mount_s3_bucket(name, path, key, secret)

        self.assertEqual(resp, None)


if __name__ == "__main__":
    unittest.main()
