import unittest

from pyfastobase_http.fastocloud import FastoCloud
from pyfastobase_http.public.auth import Auth

endpoint = "https://api.fastocloud.com"
auth = Auth("fastocloud", "fastocloud")
client = FastoCloud(endpoint=endpoint, auth=auth)


class RestartStream(unittest.TestCase):
    def test_lifetime(self):
        id = "6036824ced84ffd2c5e50db0"

        resp = client.restart_stream(id)

        self.assertEqual(resp, None)


if __name__ == "__main__":
    unittest.main()
